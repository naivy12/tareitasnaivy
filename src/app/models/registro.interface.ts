export interface RegistroInterface{
  email: string;
  name:string;
  firstname: string;
  lastname?: string;
  password: string;
}
